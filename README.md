# README

## Installation

Create a python environment (tested with Python=3.6) and install the requirements running

```
pip install -r requirements.txt
```

## Training algorithm

Reference file: "DayAhead_LP_training.py"

Check the following input parameters:
- day_timesteps (line 66): number of timesteps per day (for example, 48 if available data are with 30min resolution)
- input days (line 67): number of days that will be used for the prediction (smaller than the number of days used for the training)

To run the code using example data, run the "DayAhead_LP_training.py" as it is.

To replace the example data in input with your own data, update the datain variable defined in line 70.

**Important**: the input data **MUST** be an array of data of length "n*day_timesteps", where "n" is the number of days used for the training.

Note: the algorithm will save locally some Python objects (4 different files) containing details of the trained model. 
These files are required later by the prediction algorithm.


## Prediction algorithm

Reference file: "DayAhead_LP_prediction.py"

To run the code using example data, run the "DayAhead_LP_prediction.py" after having run the training algorithm with the example data.

To replace the example data in input with your own data, update the xpred variable defined in line 28.

**Important**: the input data **MUST** be an array of data of length "input days*timesteps", which are the parameters defined for the training.

Note: to work, the prediction algorithm needs to load the Python objects with the training models details that were created and saved by the training algorithm.

