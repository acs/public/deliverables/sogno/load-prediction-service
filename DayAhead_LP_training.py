import numpy as np
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn import preprocessing
import pickle


def datasetprep(datain, day_timesteps, num_input_days):
    timesteps = datain.shape[0]
    num_days = timesteps/day_timesteps  #number of days in the input dataset
    
    training_sets = int(num_days - num_input_days-1)
    x_training_raw = np.empty((0,day_timesteps*num_input_days))
    y_training_raw = np.empty((0,day_timesteps))

    for k in range(0,training_sets):
        xtr = datain[day_timesteps*k:day_timesteps*(k+num_input_days),]
        x_training_raw = np.vstack((x_training_raw,xtr))    # training inputs
        ytr = datain[day_timesteps*(k+num_input_days+1):day_timesteps*(k+num_input_days+2),]
        y_training_raw = np.vstack((y_training_raw,ytr))    # training outputs

    return [x_training_raw, y_training_raw]

    
def build_model(day_timesteps):
    model = Sequential()
    model.add(LSTM(50,input_shape=(None,1),return_sequences=True))
    model.add(Dropout(0.2))

    model.add(LSTM(100,return_sequences=False))
    model.add(Dropout(0.2))

    model.add(Dense(day_timesteps))
    model.add(Activation("linear"))
    model.compile(loss="mse", optimizer="rmsprop")
    return model


def run_training(datain, day_timesteps, input_days):
    epochs = 10 #6
    [xtrain, ytrain] = datasetprep(datain, day_timesteps, input_days) 
    model = build_model(day_timesteps)
    
    scale = preprocessing.MinMaxScaler()
    xtrain = scale.fit_transform(xtrain)
    xtrain = np.reshape(xtrain, (xtrain.shape[0], xtrain.shape[1], 1))
    scalerfile_x = 'scale_x.sav'
    pickle.dump(scale, open(scalerfile_x, 'wb'))
    
    ytrain = scale.fit_transform(ytrain)
    scalerfile_y = 'scale_y.sav'
    pickle.dump(scale, open(scalerfile_y, 'wb'))
    
    # training and predicting real power
    model.fit(xtrain, ytrain, batch_size=8, epochs=epochs, validation_split=0.3)
    # serialize model to JSON
    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model.h5")


# ----- PARAMETRIZATION OF ALGORITHM
day_timesteps = 48
input_days = 10

# ----- RUN ALGORITHM (replace input data by your own)
datain = np.loadtxt('sample_data/data_train.txt', delimiter=';')
run_training(datain, day_timesteps, input_days)
