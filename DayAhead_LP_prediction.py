import numpy as np
from keras.models import model_from_json
import pickle


def run_prediction(xpred):
    xpred = np.reshape(xpred, (1, xpred.shape[0]))
    scalerfile = 'scale_x.sav'
    scale = pickle.load(open(scalerfile, 'rb'))
    xpred = scale.transform(xpred)
    xpred = np.reshape(xpred, (1, xpred.shape[1], 1))
    
    # loading LSTM model
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    # load weights into the LSTM model
    model.load_weights("model.h5")

    prediction_scaled = model.predict(xpred)
    scalerfile = 'scale_y.sav'
    scale = pickle.load(open(scalerfile, 'rb'))
    prediction = scale.inverse_transform(prediction_scaled).flatten()
    return prediction

# ----- RUN ALGORITHM (replace input data by your own)
xpred = np.loadtxt('sample_data/data_pred.txt', delimiter=';')   # expected data with length of day_timesteps*input_days (parameters used during training)
prediction = run_prediction(xpred)